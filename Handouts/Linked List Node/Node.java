/**
 * Holds all of the information for one linked list node.
 *
 * @author Jason Heard
 */
public class Node {

    /**
     * The data for this node.
     */
    private int data;

    /**
     * The next node in the list. If this is {@code null}, there are no more
     * items.
     */
    private Node next;

    /**
     * Constructs a node with the given data.  The next pointer is set to {@code null}.
     *
     * @param data the data for the new node.
     */
    public Node(int data) {
        this.data = data;
        this.next = null;
    }

    /**
     * Constructs a node with the given data and next pointer.
     *
     * @param data the data for the new node.
     * @param next the node that will follow this node.
     */
    public Node(int data, Node next) {
        this.data = data;
        this.next = next;
    }

    /**
     * Gets the data in this node.
     *
     * @return the data in this node.
     */
    public int getData() {
        return this.data;
    }

    /**
     * Gets the next node in the list.
     *
     * @return the next node in the list.
     */
    public Node getNext() {
        return this.next;
    }

    /**
     * Updates the data in this node to the given value.
     *
     * @param data the new data for this node.
     */
    public void setData(int data) {
        this.data = data;
    }

    /**
     * Updates the node that follows this node with the given node.
     *
     * @param next the new node that follows this node.
     */
    public void setNext(Node next) {
        this.next = next;
    }

}
