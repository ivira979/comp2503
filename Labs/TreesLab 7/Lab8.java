public class Lab8
{
    private String[] list = {
      "Robson", "Columbia", "North Twin", "Clemenceau", "Twins Tower", 
      "Alberta", "Forbes", "Assiniboine", "South Twin", "Goodsir", 
      "Temple", "Snow Dome", "Goodsir North", "Kitchener", "Bryce", 
      "Lyell", "Edward", "Hungabee", "Rudolph", "Brazeau", 
      "Athabasca", "Victoria", "Stutfield", "King Edward", "Andromeda", 
      "Walter", "Lefroy", "Joffre", "Lunette", "Deltaform", 
      "Helmet", "Tsar", "King George", "Victoria N",  "Sir Douglas", 
      "Resplendent", "Christian", "Woolley", "Alexandra", "Whitehorn", 
      "Stutfield NE2", "Hector", "Bryce NE1", "Cromwell", "Willingdon", 
      "West Twin", "Diadem", "Rae", "Edith Cavell", "Tusk", 
      "Warren", "Cline", "Fryatt", "Harrison", "Recondite", 
      "Murchison SE", 
      "Victoria", "Robson"};

   public static void main( String[] args) 
   {
      Lab8 l = new Lab8();
      l.run();
   }

   private void run()
   {
      BST<String> bst = new BST<String>(); 
      for ( int i = 0; i < list.length; i++) 
         bst.add( list[i]);
      System.out.println( "\nInorder\n========");
      bst.inOrder();
      System.out.println( "\npreorder\n========");
      bst.preOrder();
      System.out.println( "\nPostorder\n========");
      bst.postOrder();
      System.out.println( "\nLevelorder\n========");
      bst.levelOrder();
      System.out.println( "The tree has " + bst.size() + " nodes and height " 
            + bst.height()); 
   }
}

