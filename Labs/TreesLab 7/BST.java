import java.util.Iterator;
public class BST<T extends Comparable<T>> 
{
   class BSTNode implements Comparable<BSTNode>
   {
      private T data;
      private BSTNode left; 
      private BSTNode right; 
     
      public BSTNode( T d) 
      {
         setLeft( null);
         setRight( null);
         setData( d);
      }

      public T getData() { return data; }
      public void setData( T d) { data = d; }

      public void setLeft( BSTNode l) { left = l;}
      public void setRight( BSTNode r) { right = r;}
      public BSTNode getLeft()  { return left;}
      public BSTNode getRight()  { return right;}
      public boolean isLeaf() 
      { 
         return ( getLeft() == null) && ( getRight() == null);
      }

      public int compareTo( BSTNode o) 
      {
         return this.getData().compareTo( o.getData());
      }
      
   }

   // The different traversal types. 
   public static final int INORDER = 0;
   public static final int PREORDER = 1;
   public static final int POSTORDER = 2;
   public static final int LEVELORDER = 3;

   private BSTNode root;
   private int size; 

   public BST()
   {
      root = null;
      size = 0;
   }

   /** Return true if element d is present in the tree. 
   */
   public T find( T d) 
   {
      return find( d, root); 
   }

   /** Add element d to the tree. 
   */
   public void add( T d) 
   {
      BSTNode n = new BSTNode( d); 
      if ( root == null) 
      {
         size++;
         root = n; 
      }
      else 
      {
         add( root, n); 
      }
   }

   /** Return the height of the tree. 
    */
   public int height() 
   {
      return height( root);
   }

   /** Return the number of nodes in the tree. 
    */
   public int size() { 
      return size;
   }

   public void inOrder() 
   {
      traverse( root, INORDER);
   }

   public void postOrder() 
   {
      traverse( root, POSTORDER);
   }

   public void preOrder() 
   {
      traverse( root, PREORDER);
   }

   public void levelOrder() 
   {
      traverse( root, LEVELORDER);
   }

   // Private methods. 

   private T find( T d, BSTNode r) 
   {
      if ( r == null) 
         return null;
      int c = d.compareTo( r.getData()); 
      if ( c == 0) 
         return r.getData();
      else if ( c < 0) 
         return find( d, r.getLeft());
      else 
         return find( d, r.getRight());
   } 


   /* Do the actual add of node r to tree rooted at n */
   private void add( BSTNode r, BSTNode n) 
   {
      int c = n.compareTo( r);
      if ( c < 0) 
      {
          // TO DO
        }
   }
     

   /* Implement a height method. */
   private int height( BSTNode r) 
   {
      int h = 0;
      
      // TO DO
      return h;
   } 


   /* Traverse the tree.  travtype determines the type of 
      traversal to perform. */
   private void traverse( BSTNode r, int travType) 
   {
      if ( r != null) 
      {
         switch ( travType) 
         {
            case INORDER: 
                     // TO DO
                     break;
            case PREORDER: 
                     // TO DO 
                     break;
            case POSTORDER: 
                     // TO DO
                     break;
            case LEVELORDER:
                     levelOrder( r);
                     break;
         }
      }
   }

   private void visit( BSTNode r) 
   {
       if ( r != null) System.out.println( r.getData());
   }


   /* traverse the subtree r using level order. */
   private void levelOrder( BSTNode r) 
   {
      BSTNode currNode = r;
      Queue<BSTNode> q = new Queue<BSTNode>();
      // TO DO
    
      
   }
}
