import java.util.*;

public class TestMovie {

    static ArrayList<Movie> list = new ArrayList<Movie>();
	public static void main(String[] args) {
		// TODO Auto-generated method stub
        
        list.add(new Movie("Force Awakens", 8.3, 2015));
        list.add(new Movie("Star Wars", 8.3, 1980));
        list.add(new Movie("Empire Strikes Back", 8.8, 1980));
        list.add(new Movie("Return of the Jedi", 8.8, 1983));
        list.add(new Movie("Thelma and Louise", 8.8, 1991));
        list.add(new Movie("The Sweet Hereafter", 7.6, 1997));
        list.add(null);
        list.add(null);
        list.add(new Movie("The Shining", 8.7, 1980));
        list.add(new Movie("Scarface", 8.8, 1983));
        list.add(new Movie("Cujo", 7.6, 1983));
        
        // Comparing and thenComparing methods in Java 8
        // How does the following code sort the data?
        System.out.println("------singleComparator----------");
        Comparator<Movie> singleComparator =  
        		Comparator.comparing(Movie::getRating);
        singleComparator = Comparator.nullsFirst(singleComparator);
        Collections.sort(list, singleComparator);
        display();
        
        //  How does the following code sort the data?
        System.out.println("------multiComparator----------");
        Comparator<Movie> multiComparator =  
        		Comparator.comparing(Movie::getRating).reversed().thenComparing(Movie::getName);
        multiComparator = Comparator.nullsFirst(multiComparator);
        Collections.sort(list, multiComparator);
        display();
        
        
        System.out.println("------multiComparator2----------");
        //  How does the following code sort the data? 
        Comparator<Movie> multiComparator2 =  
        		Comparator.comparing(Movie::getYear).thenComparing(Movie::getName).thenComparing(Comparator.comparing(Movie::getRating).reversed()); 
        multiComparator2 = Comparator.nullsLast(multiComparator2);
        Collections.sort(list, multiComparator2);
        display();  
        
        System.out.println("------weirdComparator----------");
        //  How does the following code sort the data? 
        Comparator<Movie> weirdComparator =  
        		Comparator.comparing(Movie::nameLength).thenComparing(Movie::getYear); 
        weirdComparator = Comparator.nullsLast(weirdComparator);
        Collections.sort(list, weirdComparator);
        display();        
        
        // Create a Comparator that orders ratings in forward order.  If they are equal, then years in reverse order.  If they are equal, then
        // names in forward order.  Nulls will go last
        
	}
	static void display() {
        for (Movie movie: list){
        	if ( movie != null )
        		System.out.println(movie.getRating() + " " +  movie.getName() + " " + movie.getYear());
        	else
        		System.out.println("null");
        }
	}
}
