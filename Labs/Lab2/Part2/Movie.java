import java.util.Comparator;

public class Movie implements Comparable<Movie>{

    public double rating;
    public String name;
    public int year;

    public int compareTo(Movie m){
        return this.year - m.year;
    }

    public Movie(String n, double r, int y) {
        this.name = n;
        this.rating = r;
        this.year = y;
    }
    public static Comparator<Movie> NameComparator = new Comparator<Movie>()
    {
        public int compare(Movie m1, Movie m2)
        {
        	return m1.getName().compareTo(m2.getName());
        }
    };
    public static Comparator<Movie> RatingComparator = new Comparator<Movie>()
    {
        public int compare(Movie m1, Movie m2)
        {
            if (m1.getRating() < m2.getRating()) return -1;
            if (m1.getRating() > m2.getRating()) return 1;
            else return 0;
        }
    };
    public static Comparator<Movie> YearComparator = new Comparator<Movie>()
    {
        public int compare(Movie m1, Movie m2)
        {
            if (m1.getYear() < m2.getYear()) return -1;
            if (m1.getYear() > m2.getYear()) return 1;
            else return 0;
        }
    };

    public double getRating() { return rating; }

    public String getName()  {  return name; }

    public int getYear()  {  return year;  }   
    public int nameLength() { return name.length(); }
}




