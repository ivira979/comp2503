/** Node class starting code. 
 *  COMP 2503 Winter 2018
 */
public class Node
{
   
    private Object data;
    private Node next;
    
    /**
     * Constructor for objects of class Node
     */
    public Node()
    {
       data = null;
       next = null;
    }

    public Object getData() { return data;}
    public void setData(Object o) { data = o;}
    
    public Node getNext() { return next;}
    public void setNext(Node n) { next = n;}
 
    public String toString() 
    {
        return "Node: " + getData().toString();
    }
}
