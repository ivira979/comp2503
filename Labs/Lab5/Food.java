
public class Food {
	private int count;
	private String name;
	public Food(String s, int quantity)
	{
		name = s;
		count = quantity;
	}
	public String getName()
	{
		return name;
	}
	public int getCount()
	{
		return count;
	}
	public String toString()
	{
		return name +"," +count;
	}
}
