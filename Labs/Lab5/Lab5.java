import java.util.Comparator;

public class Lab5 {

	public static void main(String[] args) {
		String array[]= {"cake","cookie","wine","bread"};
		SLList<Food> list1 = new SLList();
		SLList<Food> list2 = new SLList();

		for (int i = 0; i < array.length; i++)
		{
			Node<Food> n = new Node(new Food(array[i],0));
			list1.add(n, Comparator.comparing(Food::getName));
			Node<Food> n2 = new Node(new Food(array[i],0));
			list2.add(n2, Comparator.comparing(Food::getName).reversed());
		}
		System.out.println(list1 +"-------");
		System.out.println(list2);
		

	}

}
