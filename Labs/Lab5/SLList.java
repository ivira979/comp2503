import java.util.Comparator;

public class SLList<T> {
	private Node<T> head;
	private Node<T> tail;
	public SLList(){
		head = null;
		tail = null;
	}
	public void add(Node<T> n, Comparator c)
	{
		// empty list
		if (head == null)
		{
		}
		// if n.data is less than head.data,  add to beginning of list
		else if ( c.compare(n.data, head.data) <= 0 )
		{
		}
		// search through list for curr.data < n.data   and curr.next.data > n.data
		else 
		{
			int i = 0;
			Node<T> curr = head;
			// a while loop that stops when curr.next.data is bigger than n.data
			while (curr.next!=null && c.compare ...)
			{
				...
			}
			if (n.next == null)
			{
				// add to end of list
			}
			else
			{
				// add between curr and curr.next
			}
		}
	}
	   public void addHead( Node<T> n) 
	   {
	       if (head == null) // empty list 
	       {
	           head = n; tail = n;
	       }
	       else 
	       {
	           n.next=head;
	           head = n;
	       }
	   }
	   public void addTail( Node<T> n) 
	   {
	       if (tail == null) // list is empty
	       {
	           head = n; 
	           tail = n;
	       }
	       else 
	       {
	          tail.next = n;
	          tail = n;
	       }
	   }

	public String toString()
	{
		String str = "";
		Node<T> curr = head;
		while ( curr != null ) 
		{
			str += curr.data + "\n";
			curr = curr.next;
		}
		return str;
	}
}
