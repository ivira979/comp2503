

public class Node<T>
{
	
	public T data;
	public Node<T> next;
	
	/**	
	 * Constructor for objects of class Node
	 */
	public Node(T d)
	{
		data = d;
		next = null;
	}
	public String toString()
	{
		return data.toString();
	}
}


