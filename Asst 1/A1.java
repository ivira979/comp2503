import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * COMP 2503 Fall 2018 Assignment 1
 *
 * A1 is a processor class that parses a recipebook file from stdin and prints
 * statistics of the food used therein to stdout.
 *
 * @author *****
 * @version 1.0
 */
public final class A1 {

	private List<Food> foodList;

	/**
	 * Default constructor that initializes foodList with basic food items.
	 */
	public A1() {
		foodList = new ArrayList<>();

		for (String name : Food.BASIC_FOOD_NAMES) {
			foodList.add(new Food(name));
		}
	}

	/**
	 * Entry point of the program. Invokes the run method.
	 *
	 * @param args Command line arguments
	 */
	public static void main(String[] args) {
		new A1().run();
	}

	/**
	 * Parses a recipebook from stdin and then prints the statistics of that
	 * recipebook.
	 */
	public void run() {
		sortListAndParseRecipeBook();

		printStatistics();
	}

	/**
	 * Sorts the food list by length and then by name. Reads a recipebook file from
	 * stdin and parses the ingredient lines.
	 *
	 */
	public void sortListAndParseRecipeBook() {
		Collections.sort(foodList, Food.LENGTH_CASE_INSENSITIVE_ORDER);

		try (Scanner input = new Scanner(System.in)) {
			while (input.hasNextLine()) {
				String recipeLine = input.nextLine();

				if (recipeLine.equals("---")) {
					recipeLine = input.nextLine();

					do {
						updateAndSort(recipeLine);
					} while (!((recipeLine = input.nextLine()).equals("---")));
				}
			}
		}
	}

	/**
	 * Print statistics of the food list.
	 *
	 * Statistics printed are total ingredients, frequency of igredients, and
	 * ingredients by name.
	 */
	public void printStatistics() {
		final String delimiter = String.join("", Collections.nCopies(39, "-"));

		printTotal();

		System.out.println(delimiter);

		sortAndPrintByFrequency();

		System.out.println(delimiter);

		sortAndPrintByName();
	}

	/**
	 * Updates the food list by parsing the supplied recipe line. If the food item
	 * already exists in the foodList its frequency is increased by 1. Otherwise the
	 * food item is added to the foodList with a frequency of 1.
	 *
	 * The foodlist is sorted by length.
	 *
	 * @param recipeLine A String that is a recipe line
	 */
	private void updateAndSort(String recipeLine) {
		boolean isMatchFound = false;

		for (Food food : foodList) {
			if (recipeLine.toLowerCase().contains(food.getName())) {
				food.incrementFrequency();
				isMatchFound = true;
				break;
			}
		}

		if (!isMatchFound) {
			String foodName = extractName(recipeLine);

			if (!foodName.isEmpty()) {
				foodList.add(new Food(foodName, 1));

				Collections.sort(foodList, Food.LENGTH_CASE_INSENSITIVE_ORDER);
			}
		}
	}

	/**
	 * Extracts the food name from a recipe line. The recipe line is converted into
	 * lowercase, leading and trailing whitespaces are trimmed and suffixes are
	 * removed.
	 *
	 * @param recipeLine A String that is a recipe line
	 * @return a String that is the short food name
	 */
	private String extractName(String recipeLine) {
		recipeLine = recipeLine.toLowerCase().trim();

		recipeLine = recipeLine.substring(recipeLine.lastIndexOf(' ') + 1);

		if (recipeLine.length() > 1) {
			if (recipeLine.endsWith("es")) {
				recipeLine = recipeLine.substring(0, recipeLine.lastIndexOf("es"));
			} else if (recipeLine.endsWith("s") && !recipeLine.endsWith("ss")) {
				recipeLine = recipeLine.substring(0, recipeLine.lastIndexOf("s"));
			}
		}

		return recipeLine;
	}

	/**
	 * Prints the number of ingredients in the food list with a non-zero frequency.
	 */
	private void printTotal() {
		int count = countIngredients();

		System.out.printf("The recipe book has %d different ingredients%n", count);
	}

	/**
	 * Prints the food list alphabetically for those items with frequency > 0. The
	 * food list is sorted by name alphabetically.
	 */
	private void sortAndPrintByName() {
		Collections.sort(foodList);

		System.out.println("The complete list of foods in the recipe book, in alphabetical order");

		for (Food food : foodList) {
			if (food.getFrequency() > 0) {
				System.out.printf("(%s,%d)%n", food.getName(), food.getFrequency());
			}
		}
	}

	/**
	 * Print the top 10 (or fewer) ingredients occurring in the food list. The food
	 * list is sorted by frequency and then alphabetically.
	 */
	private void sortAndPrintByFrequency() {
		Collections.sort(foodList, Food.FREQUENCY_CASE_INSENSITIVE_ORDER);

		final int maxCount = 10;

		int count = Math.min(countIngredients(), maxCount);

		System.out.printf("Top %d ingredients, from most common to least common%n", count);

		for (int i = 0; i < count; i++) {
			Food food = foodList.get(i);

			System.out.printf("(%s,%d)%n", food.getName(), food.getFrequency());
		}
	}

	/**
	 * Counts the number of foods in the food list that have a frequency > 0.
	 *
	 * @return an int that is the count of food
	 */
	private int countIngredients() {
		int count = 0;

		for (Food food : foodList) {
			if (food.getFrequency() > 0) {
				count++;
			}
		}

		return count;
	}
}
