import java.util.Comparator;

/**
 * COMP 2503 Fall 2018 Assignment 1
 *
 * A class representing a food item.
 *
 * @author *****
 * @version 1.0
 */
public class Food implements Comparable<Food> {

	public static final String[] BASIC_FOOD_NAMES = { "baking powder", "baking soda", "broth", "tomato paste",
			"tomato sauce", "tomato", "flour", "egg", "garlic", "cheese", "rice", "onion", "salt", "pepper", "vinegar",
			"carrot", "sweet potato", "potato", "cream", "milk", "bean", "green bean", "beef", "chicken", "cumin",
			"basil", "oregano", "oil", "fish" };

	public static final Comparator<Food> FREQUENCY_CASE_INSENSITIVE_ORDER = Comparator.comparing(Food::getFrequency)
			.reversed().thenComparing(Food::getName, String.CASE_INSENSITIVE_ORDER);

	public static final Comparator<Food> LENGTH_CASE_INSENSITIVE_ORDER = Comparator
			.comparing((Food f) -> f.name.length()).reversed()
			.thenComparing(Food::getName, String.CASE_INSENSITIVE_ORDER);

	private String name;

	private int frequency;

	/**
	 * Constructor that creates a food object with the supplied name and a frequency
	 * of 0.
	 *
	 * @param name The name of the food object.
	 */
	public Food(String name) {
		this(name, 0);
	}

	/**
	 * Constructor that creates a food object with the supplied name and frequency.
	 *
	 * @param name      A String that is the name of the food object.
	 * @param frequency An int that is the frequency of the food object.
	 */
	public Food(String name, int frequency) {
		this.name = name;
		this.frequency = frequency;
	}

	public final String getName() {
		return name;
	}

	public final int getFrequency() {
		return frequency;
	}

	/**
	 * Increment frequency of this food object by 1.
	 */
	public void incrementFrequency() {
		frequency++;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Food food) {
		return getName().compareToIgnoreCase(food.getName());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + frequency;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj == null) {
			return false;
		}

		if (!(obj instanceof Food)) {
			return false;
		}

		Food other = (Food) obj;

		if (name != other.name) {
			return false;
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Food [name=" + name + ", frequency=" + frequency + "]";
	}
}
