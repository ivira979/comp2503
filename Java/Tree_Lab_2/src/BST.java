
public class BST 
{

	
	protected Node root;
	
	BST()
	{
		root = null;
		
	}
	
	void printPostOrder(Node node)
	{
		
		if(node == null)
		{
			return;
		}
		
		printPostOrder(node.left);
		
		printPostOrder(node.right);
		
		System.out.println(node.data + " ");
		
	}
	
	void printInOrder(Node node)
	{
		
		if(node == null)
		{
			return;
		}
		
		printInOrder(node.left);
		
		System.out.println(node.data + " ");
		
		printInOrder(node.right);
	}
	
	void printPreOrder(Node node)
	{
		
		if(node == null)
		{
			return;
		}
		
		System.out.println(node.data + " ");
		
		printPreOrder(node.left);
		
		printPreOrder(node.right);
	}
	
	
	void printPostOrder() 
	{
		printPostOrder(this.root);
	}
	
	void printPreOrder() 
	{
		printPreOrder(this.root);
	}
	
	void printInOrder() 
	{
		printInOrder(this.root);
	}
	public static void main(String[] args) 
	{
		BST tree = new BST();
		tree.root = new Node(1);
		tree.root.left = new Node(2);
		tree.root.right = new Node(3);
		tree.root.left.left = new Node(4);
		tree.root.left.right = new Node(5);
		
		
		System.out.println("Preorder trav of bin tree is: ");
		tree.printPreOrder();
		
		System.out.println("Postorder trav of bin tree is: ");
		tree.printPostOrder();
		
		System.out.println("Inorder trav of bin tree is: ");
		tree.printInOrder();
	}

}
