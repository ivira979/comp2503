import java.util.Arrays;
import java.util.Random;

/**
 * 
 */

/**
 * @author User
 *
 */
public class MergeSort {

	/**
	 * @param <T>
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] A = new int[20]; //could be changed to Array list to allow for dynamic size
		Random rand = new Random();
		int start = 0;
		int end = A.length - 1;
		
		for(int i = 0; i < 20; i++) {
			
			A[i] = rand.nextInt(100);
		}
		System.out.println(Arrays.toString(A));

		
		sort(A, start, end);
		
		System.out.println(Arrays.toString(A));
		
		
	}

	
	public static void sort(int[] Arr, int start, int end) {
		int middle = 0;
		
		if(start < end) {
			
			middle = (start + end) / 2;
			
			sort(Arr, start, middle);
			sort(Arr, middle + 1, end);
			merge(Arr, start, middle, end);
		}
		
		
	}


	private static void merge(int[] arr, int start, int middle, int end) {
		// TODO Auto-generated method stub
		int sizeOfLeft = middle - start + 1;
		int sizeOfRight = end - middle;
		
		int[] left = new int[sizeOfLeft];
		int[] right = new int[sizeOfRight];
		
		for (int i = 0; i < sizeOfLeft; i++) 
		{
			left[i] = arr[start + i]; 
		}
		for (int j = 0; j < sizeOfRight; j++) 
		{
			right[j] = arr[middle + 1 + j];
		}
		
		int i = 0;
		int j = 0;
		for (int k = start; k <= end; k++) 
		{
			if ((j >= sizeOfRight) || (i < sizeOfLeft && left[i] <= right[j])) 
			{
				arr[k] = left[i];
				i++;
			} 
			else 
			{
				arr[k] = right[j];
				j++;
			}
		}
	}
}
