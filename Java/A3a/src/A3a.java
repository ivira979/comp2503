/**
 * Name:		Irfan Virani	
 * Email:		ivira979@mtroyal.ca
 * Course:		COMP 2503
 * Instructor:	Sajida Faiyaz
 * Assignment:	3
 * Part:		B
 * Due Date:	February 18, 2019
 */
import java.util.*;

/**
 * The Class A3a.
 */
public class A3a 
{
	/** The Constant NO_INPUTS_ERR. */
	private static final String NO_INPUTS_ERR = "No inputs given! Try again!";

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) 
	{
		Scanner in = new Scanner(System.in);	

		printPrompt();
		String inputAsString = in.nextLine();
		
		while(inputAsString.length() == 0 ) // forces the user to provide at least 1 input
		{
			System.err.println(NO_INPUTS_ERR);
			inputAsString = in.nextLine();
		}
		
		in.close();
	
		ArrayList<String> inputAsStringList = new ArrayList<String>(Arrays.asList(inputAsString.split(" ")));
		
		A3a a = new A3a(inputAsStringList);
	} 
	
	/**
	 * Instantiates a new A3a. Initializes a inputStack, the reverse and
	 * a temporary Stack. Performs the comparison between the input Stack
	 * and the reverse and prints out the corresponding result.
	 * 
	 * Prints True if the input is a palindrome (input == reverse).
	 * Prints False otherwise.
	 *
	 * @param inputAsStringList the input as an ArrayList of Strings
	 */
	public A3a (ArrayList<String> inputAsStringList) 
	{
		for(int i = 0; i < inputAsStringList.size(); i++) 
		{
			Stack<Character> inputAsStack = new Stack<Character> ();
			Stack<Character> reverse = new Stack<Character> ();
			Stack<Character> temp = new Stack<Character> ();
			
			for(int j = 0; j < (inputAsStringList.get(i)).length(); j++) 
			{
				inputAsStack.push(inputAsStringList.get(i).charAt(j));
			}
					
			int size = inputAsStack.size();
			
			for(int j = 0; j < size; j++) 
			{
				temp.push(inputAsStack.peek());	
				reverse.push(inputAsStack.pop());
			}
			
			size = temp.size();
			
			for(int j = 0; j < size; j++) 
			{
				inputAsStack.push(temp.pop());
			}
			
			if(inputAsStack.equals(reverse)) 
			{
				System.out.print(toString(reverse));
				System.out.println(" : Yes");
			}
			else
			{
				System.out.print(toString(reverse));
				System.out.println(" : No");
			}
		}
	}
	
	/**
	 * To string.
	 *
	 * @param charStack the stack of Characters
	 * @return the charStack as a String
	 */
	public static String toString(Stack<Character> charStack) 
	{
		String str = "";
		int size = charStack.size();
		
		for(int i = 0; i < size; i++) 
		{
			str += charStack.pop();
			
		}
		
		return str;
	}
	
	/**
	 * Prints the prompt welcoming the user and requesting the input.
	 */
	public static void printPrompt() 
	{
		System.out.println("Welcome to Palindrome Panic!" + System.lineSeparator());
		System.out.print("Please enter any number of words, seperated by whitespace, followed by the ENTER/RETURN key: ");
	}
}
