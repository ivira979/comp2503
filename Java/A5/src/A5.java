/**
 * Name:		Irfan Virani	
 * Email:		ivira979@mtroyal.ca
 * Course:		COMP 2503
 * Instructor:	Sajida Faiyaz
 * Assignment:	5
 * Due Date:	March 31, 2019
 */

import java.util.*;
import java.io.*;

/**
 * The Class A5.
 */
public class A5 
{
	/** The word list. */
	private Hashtable<String, Integer> wordList = new Hashtable<>();
	
	/** The Constant WORDLIST_PROMPT. */
	private static final String WORDLIST_PROMPT = "Please enter a filename for the input: ";
	
	/** The Constant WORDSTATISTICS_PROMPT. */
	private static final String WORDSTATISTICS_PROMPT = "Please enter a filename for the output: ";
	
	/** The set of all keys in the hash table. */
	private ArrayList<String> keySet = new ArrayList<>(); 
	
	/** The list of the k max words by word count. */
	private ArrayList<String> maxlist = new ArrayList<>();
	
	/** The key, value list. */
	private ArrayList<String> kvList = new ArrayList<>();
	
	/** The sum of all words and occurrences in the word list. */
	private Integer sum = 0;
	
	/** The PrintWriter for the file. */
	private static PrintWriter outToFile = null;
	
	
	/**
	 * Instantiates a new a5.
	 */
	public A5() 
	{
		wordList.clear();
		keySet.clear();
		maxlist.clear();
		kvList.clear(); 
	}
	
	/**
	 * Instantiates a new a5 by loading the wordList with the given keys(words) from a file.
	 *
	 * @param listFromFile the list from a file
	 */
	public A5(ArrayList<String> listFromFile)
	{
		for (String w: listFromFile)
		{
			this.load(w);
		}
	}

	/**
	 * The main method.
	 * 
	 * Prompts the user for a filename and reads the contents of the file to an ArrayList.
	 * The input is filtered to ensure that all characters are lower case and do not contain
	 * numeric or special characters. The "words" are then loaded into the hash table as keys
	 * with the appropriate count. A series of operations are then performed to determine the 
	 * number of total words as well as the top 10 words by count.
	 * 
	 * The keys and their corresponding values are then written to the user specified filename
	 * as well as the top 10 list.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) 
	{
		Scanner in = new Scanner(System.in);
		
		String outfile = null;
		String infile = null;
		
		ArrayList<String> listFromFile = new ArrayList<>();
		
		Scanner wordReader = null;
		
		boolean success = false;

		printWelcome();
		
		System.out.print(WORDLIST_PROMPT);
		infile = in.nextLine();
		
		while(!success)
		{
			try 
			{
				wordReader = new Scanner(new File(infile));
				success = true;
			} 
			catch (FileNotFoundException e1) 
			{
				System.err.print("File not found! Try again: "); 
				infile = in.nextLine();
			}
		}
		
		while(wordReader.hasNextLine())
		{
			String next = wordReader.nextLine();
			next = next.toLowerCase().replaceAll("[^a-z ]", "");
			listFromFile.addAll(Arrays.asList(next.split(" ")));
		}
		
		wordReader.close();
		
		while(listFromFile.contains(" ") || listFromFile.contains(""))
		{
			listFromFile.remove(" ");
			listFromFile.remove("");
		}
		
		System.out.print(WORDSTATISTICS_PROMPT);
		outfile = in.nextLine();
		
		in.close();
		
		do
		{
			try 
			{
				outToFile = new PrintWriter(new BufferedWriter(new FileWriter(outfile)));
				success = true;
			} 
			catch (IOException e) 
			{
				System.err.println("File cannot be opened!");
				outfile = in.nextLine();
				success = false;
			}
		}while(!success);
		
		A5 dictionary = new A5(listFromFile);
		
		dictionary.exportKeys();
		dictionary.exportSum();	
		dictionary.toFile("Total Words in the file: " + dictionary.sum);
		
		listFromFile.clear();
		listFromFile = null;
		
		dictionary.exportKV();
		
		Collections.sort(dictionary.kvList);
		
		for(String kv: dictionary.kvList) 
		{
			dictionary.toFile(kv);
		}
		
		dictionary.topTenMax();
		dictionary.toFile("");
		
		dictionary.toFile("Top 10 Words in the file: ");
		
		for(String key: dictionary.maxlist)
		{
			dictionary.toFile(key + " : " + dictionary.getValue(key));
		}
		
		outToFile.close();
		outToFile.flush();

	}

	
	/**
	 * Export all keys from hash table.
	 */
	public void exportKeys()
	{
		this.keySet.addAll(this.wordList.keySet());
	}
	
	/**
	 * Increments the sum by the count of the key.
	 *
	 * @param key the key
	 * @return the integer
	 */
	public Integer incSum(String key)
	{
		Integer toAdd = this.getValue(key);
		this.sum += toAdd;
		
		return toAdd;
	}
	
	/**
	 * Exports the total sum.
	 */
	public void exportSum()
	{
		for(String key: this.keySet)
		{
			this.incSum(key);
		}
	}
	
	/**
	 * Gets the value(count) at the given key(word).
	 *
	 * @param key the key
	 * @return the value
	 */
	public Integer getValue(String key)
	{
		return this.wordList.get(key);
	}
	
	/**
	 * Gets the size of the list.
	 *
	 * @return the size
	 */
	public Integer getSize()
	{
		return this.wordList.size();
	}
	
	/**
	 * Key of maximum value in the list(word with the greatest count).
	 *
	 * @return the string
	 */
	public String keyOfMax()
	{
		String key = null, currKey = null;
		Integer max = 0, currMax = 0;
		
		for(String s: this.keySet)
		{
			currKey = s;
			currMax = this.wordList.get(s);
			
			if(currMax.compareTo(max) == 0)
			{
				if(currKey.compareTo(key) < 0)
				{
					key = currKey;
				}
			}
			else
				if(currMax.compareTo(max) > 0)
				{
					max = currMax;
					key = currKey;
				}
		}
		
		return key;
	}
	
	/**
	 * Generates the list of the words with the top ten max counts. 
	 */
	public void topTenMax()
	{
		ArrayList<String> maxlist = new ArrayList<>();
		
		int size = this.keySet.size();
		
		if(this.keySet.size() >= 10)
		{
			for(int i = 0; i < 10; i++)
			{
				maxlist.add(this.keyOfMax());
				this.keySet.remove(this.keyOfMax());
			}
		}
		else
		{
			for(int i = 0; i <= size; i++)
			{
				maxlist.add(this.keyOfMax());
				this.keySet.remove(this.keyOfMax());
			}
		}
		
		while(maxlist.contains(null))
		{
			maxlist.remove(null);
		}
		
		this.maxlist.addAll(maxlist);
		
		return;
	}
	
	/**
	 * Load the hash table with the given key(word). 
	 * If the key already exists, the count is incremented.
	 * If the key does not exist, it is added with a count of 1.
	 *
	 * @param key the key
	 */
	public void load(String key)
	{
		try
		{
			wordList.put(key, wordList.get(key) + 1);
		}
		catch(NullPointerException e)
		{
			wordList.putIfAbsent(key, 1);
		}
	}
	
	/**
	 * Export the keys and their values into a list for easier writing to a file.
	 */
	public void exportKV()
	{
		for(String key: this.keySet)
		{
			kvList.add(key + " : " + this.getValue(key));
		}
	}
	
	/**
	 * Writes the specified String to the file.
	 *
	 * @param str the str
	 */
	public void toFile(String str)
	{
		outToFile.write(str + "\n");
	}

	
	/**
	 * Prints the welcome message.
	 */
	public static void printWelcome()
	{
		System.out.println("Welcome to Assignment 5: WORD STATISTICS");
	}
	
}
