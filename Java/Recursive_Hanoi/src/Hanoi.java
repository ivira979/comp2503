/**
 * 
 */

import java.util.*;
/**
 * @author User
 *
 */
public class Hanoi {

	public static Stack<Integer> A = new Stack<Integer>();
	public static Stack<Integer> B = new Stack<Integer>();
	public static Stack<Integer> C = new Stack<Integer>();
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner in = new Scanner(System.in);
		
		int size = in.nextInt();
		
		setup(size);
		
//		System.out.println("After setup?");
//		System.out.println(A.toString());
//		System.out.println(B.toString());
//		System.out.println(C.toString());
//		
//
//		move(size, A, C, B);
//		
//		System.out.println("After Hanoi?");
//		System.out.println(A.toString());
//		System.out.println(B.toString());
//		System.out.println(C.toString());
		
		
		move(size, 'A', 'C', 'B');

		
		in.close();
		
	}
	
	public static void setup(int n) {
		
		
		
		if(n > 0) {
			
			for(int i = n; i > 0; i--) {
				
				A.push(i);
			}
			
		}
		
	}
	
	public static <T> void move(int size, Stack<T> A, Stack<T> C, Stack<T> B) {
		
		if(size > 0)
		{
			move(size-1, A, B, C);
			
			try 
			{
				C.push(A.pop());
			}
			catch(EmptyStackException e) 
			{
				
			}
			
			move(size-1, B, C, A);	
		}
	}
	
	public static void move(int size, char from, char to, char using) {
		
		if(size == 1)
		{
			System.out.println("Moving disc " + size + " from " + from + " to " + to);
		}
		else
		{
			move(size - 1, from, using, to);
			System.out.println("Moving disc " + size + " from " + from + " to " + to);
			move(size - 1, using, to, from);
		}
	}
	

}
