
public class Node {

	private int data;
	private Node left;
	private Node right;
	private Node root;
	private boolean visited;
	
	public Node() {
		
		this.data = 0;
		this.left = null;
		this.right = null;
		this.root = null;
		this.visited = false;
	}
	
	public Node(int data) {

		this();
		this.data = data;
		

	}

	public int getData() {
		return data;
	}

	public void setData(int data) {
		this.data = data;
	}

	public Node getLeft() {
		return left;
	}

	public void setLeft(Node left) {
		this.left = left;
	}

	public Node getRight() {
		return right;
	}

	public void setRight(Node right) {
		this.right = right;
	}

	public Node getRoot() {
		return root;
	}

	public void setRoot(Node root) {
		this.root = root;
	}

	public boolean isVisited() {
		return visited;
	}

	public void setVisited(boolean visited) {
		this.visited = visited;
	}
	
}
