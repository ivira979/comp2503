/**
 * Name:		Irfan Virani	
 * Email:		ivira979@mtroyal.ca
 * Course:		COMP 2503
 * Instructor:	Sajida Faiyaz
 * Assignment:	4
 * Part:		A, B, C
 * Due Date:	March 13, 2019
 */

/**
 * The Class BST.
 * 
 * A Java representation of the Binary Search Tree Data Structure in which the Node(s)
 * are Integers.
 * 
 */

public class BST 
{
	/** The root of the Binary Search Tree. */
	private Node<Integer> root;
	
	/** The size of the Binary Search Tree. */
	private int size;
	
	/** The sum of the Nodes in the Binary Search Tree. */
	private int sum;
	
	/**
	 * Instantiates a new Binary Search Tree (BST).
	 */
	BST()
	{
		this.root = null;
		this.size = 0;
		this.sum = 0;
	}
	
	/**
	 * Recursively gets the size of the BST by its subtrees.
	 *
	 * @param node the root node of the subtree
	 * @return the size of the tree
	 */
	public int size(Node<Integer> node)
	{
		if(node == null) 
		{
			return 0;
		}

		size(node.getLeft());
		size(node.getRight());
		
		return size += 1;
	}
	
	/**
	 * Recursively gets the sum of the BST by its subtrees.
	 *
	 * @param node the root node of the subtree
	 * @return the sum of the nodes of the tree
	 */
	public int sum(Node<Integer> node)
	{
		
		if(node == null) 
		{
			return 0;
		}

		sum(node.getLeft());
		sum(node.getRight());
		
		return sum += node.getData();
	}
	
	/**
	 * Gets the size of the BST by calling the size(Node<Integer>) method with the root of the tree.
	 *
	 * @return the size of the BST
	 */
	public int getSize()
	{
		this.size = size(this.root);
		return this.size;
	}
	
	/**
	 * Gets the sum of the Nodes of the BST by calling the sum(Node<Integer>) method with the root of the tree.
	 *
	 * @return the sum of the nodes of the BST
	 */
	public int getSum()
	{
		this.sum = sum(this.root);
		return this.sum;
	}
	
	/**
	 * Prints the post order traversal path of the BST.
	 *
	 * @param node the root node of the subtree
	 */
	void printPostOrder(Node<Integer> node)
	{
		if(node == null)
		{
			return;
		}
		
		printPostOrder(node.getLeft());
		
		printPostOrder(node.getRight());
		
		System.out.print(node.getData() + " ");
	}
	
	/**
	 * Prints the in order traversal path of the BST.
	 *
	 * @param node the root node of the subtree
	 */
	void printInOrder(Node<Integer> node)
	{
		
		if(node == null)
		{
			return;
		}
		
		printInOrder(node.getLeft());
		
		System.out.print(node.getData() + " ");
		
		printInOrder(node.getRight());
	}
	
	/**
	 * Prints the pre order traversal path of the BST.
	 *
	 * @param node the root node of the subtree
	 */
	void printPreOrder(Node<Integer> node)
	{
		
		if(node == null)
		{
			return;
		}
		
		System.out.print(node.getData() + " ");
		
		printPreOrder(node.getLeft());
		
		printPreOrder(node.getRight());
	}
	
	/**
	 * Calls the printPostOrder(Node<Integer>) method with the root of the BST.
	 */
	void printPostOrder() 
	{
		printPostOrder(this.root);
	}
	
	/**
	 * Calls the printInOrder(Node<Integer>) method with the root of the BST.
	 */
	void printInOrder() 
	{
		printInOrder(this.root);
	}
	
	/**
	 * Calls the printPreOrder(Node<Integer>) method with the root of the BST.
	 */
	void printPreOrder() 
	{
		printPreOrder(this.root);
	}
	
	/**
	 * Adds the specified Integer value to the BST while adhering to the principles of the BST structure.
	 * This method uses the add(Integer) method to facilitate this.  
	 * If the value to add is already present in the BST, then the appropriate error message is printed and
	 * the value is skipped.
	 * 
	 * @param i the Integer to add
	 */
	void add(Integer i)
	{
		if(this.root == null) 
		{
			root = new Node<Integer>(i);			
		}
		else
		{
			root.add(i);
		}
	}
}
