/**
 * Name:		Irfan Virani	
 * Email:		ivira979@mtroyal.ca
 * Course:		COMP 2503
 * Instructor:	Sajida Faiyaz
 * Assignment:	4
 * Part:		A, B, C
 * Due Date:	March 13, 2019
 *
 *
 **/

/**
 * The Class Node.
 * 
 * @param <T> the generic type
 **/

public class Node<T extends Comparable<T>> 
{
	
	/** 
	 * The data encapsulated in the Node. 
	 * The data type is generic.
	 */
	private T data;
	
	/** The left and right children of the Node. */
	private Node<T> left, right;
	
	/**
	 * Instantiates a new node with specified data value.
	 *
	 * @param d the data value to instantiate the Node with.
	 */
	Node(T d)
	{
		this.data = d;
		this.left = this.right = null;
	}

	/**
	 * Gets the data stored in the Node.
	 *
	 * @return the data stored in the Node.
	 */
	public T getData() 
	{
		return data;
	}

	/**
	 * Sets the data in the Node.
	 *
	 * @param data the data to set.
	 */
	public void setData(T data) 
	{
		this.data = data;
	}

	/**
	 * Gets the left child of the Node.
	 *
	 * @return the left child of the Node.
	 */
	public Node<T> getLeft() 
	{
		return left;
	}

	/**
	 * Sets the left child of the Node.
	 *
	 * @param left the Node to set as the left child.
	 */
	public void setLeft(Node<T> left) 
	{
		this.left = left;
	}

	/**
	 * Gets the right.
	 *
	 * @return the right child of the Node.
	 */
	public Node<T> getRight() 
	{
		return right;
	}

	/**
	 * Sets the right child of the Node.
	 *
	 * @param right the Node to set as the right child.
	 */
	public void setRight(Node<T> right) 
	{
		this.right = right;
	}

	/**
	 * Adds the value to the subtree.
	 *
	 * @param value the value to add.
	 */
	public void add(T value) 
	{
		int result = (this.getData()).compareTo(value);
		
		if(result == 0) 
		{
			System.err.println("Node " + value + " already exists in BST!");	
		}
		else
			if(result > 0)
			{
				if(this.getLeft() == null) 
				{
					this.setLeft(new Node<T>(value));
				}
				else
				{
					(this.getLeft()).add(value);
				}
			}
			else
				if(result < 0)
				{
					if(this.getRight() == null) 
					{
						this.setRight(new Node<T>(value));
					}
					else
					{
						(this.getRight()).add(value);
					}
				}
	}
}
