/**
 * Name:		Irfan Virani	
 * Email:		ivira979@mtroyal.ca
 * Course:		COMP 2503
 * Instructor:	Sajida Faiyaz
 * Assignment:	4
 * Part:		A
 * Due Date:	March 13, 2019
 */

import java.util.*;

/**
 * The Class A4a.
 */
public class A4a 
{	
	
	/** The binary search tree. */
	private BST binarySearchTree;
	
	/** The Constant INPUT_PROMPT. */
	public static final String INPUT_PROMPT = "Please input any number of integers with a space between "
		+ "each followed by the ENTER/RETURN key: ";
	
	/**
	 * Instantiates a new A4a with the input ArrayList of Integers.
	 *
	 * @param input the input ArrayList of Integers
	 */
	A4a(ArrayList<Integer> input)
	{
		this.binarySearchTree = new BST();
		
		for(int i: input)
		{
			binarySearchTree.add(i);
		}
	}
	
	/**
	 * The main method.
	 * 
	 * Reads a series of user Integer values from the console as a String.
	 * 
	 * The String is purged of any non-Integer values and if no integers are entered
	 * then the user is asked for input again.
	 * 
	 * The String is then processed into an Array of the substrings derived
	 * when the String is split when a " " is encountered.
	 *  
	 * The Array of substrings is converted to an ArrayList for ease of processing and an
	 * ArrayList of Integers is formed.
	 * 
	 * The input is then passed to the constructor and a A4a object is instantiated which in turn
	 * builds the BST.
	 * 
	 * The input list is then purged of duplicates and displayed for the user to see, followed by
	 * the inorder, postorder, and preorder traversals of the BST.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) 
	{
		Scanner in = new Scanner(System.in);
		
		ArrayList<Integer> input = new ArrayList<>(Arrays.asList(50,20,75,98,80,31,150,39,23,11,77));
			
		in.close();
		
		A4a a = new A4a(input);
		
		input = a.clearDupes(input);
		
		System.out.println("Numbers inputted (excluding duplicates): " + input.toString() + System.lineSeparator());
		a.getTraversalPath();
	}
	
	/**
	 * Gets the traversal paths of the BST and prints them to the Console.
	 *
	 */
	public void getTraversalPath()
	{
		System.out.print("In order traversal: ");
		this.binarySearchTree.printInOrder();
		System.out.println(System.lineSeparator());
		
		System.out.print("Post order traversal: ");
		this.binarySearchTree.printPostOrder();
		System.out.println(System.lineSeparator());
		
		System.out.print("Pre order traversal: ");
		this.binarySearchTree.printPreOrder();
	}
	
	/**
	 * Gets the sum of the Nodes of the BST by calling the BST.getSum method.
	 *
	 * @return the sum of the Nodes of the BST.
	 */
	public int getSum()
	{
		return this.binarySearchTree.getSum();
	}
	
	/**
	 * Gets the size of the BST by calling the BST.getSize method.
	 *
	 * @return the size of the BST.
	 */
	public int getSize()
	{
		return this.binarySearchTree.getSize();
	}
	
	/**
	 * Clears duplicate values from the input list by leveraging the properties of a LinkedHashSet.
	 *
	 * @param input the input ArrayList of Integers
	 * @return the array list of Integers after it is cleared of duplicate values.
	 */
	public ArrayList<Integer> clearDupes(ArrayList<Integer> input)
	{
		Set<Integer> inputNoDupes = new LinkedHashSet<> ();
		
		inputNoDupes.addAll(input);
		input.clear();
		input.addAll(inputNoDupes);
		
		return input;
	}
}
