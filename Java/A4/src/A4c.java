/**
 * Name:		Irfan Virani	
 * Email:		ivira979@mtroyal.ca
 * Course:		COMP 2503
 * Instructor:	Sajida Faiyaz
 * Assignment:	4
 * Part:		C
 * Due Date:	March 13, 2019
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;


/**
 * The main method.
 * 
 * Reads a series of user Integer values from the console as a String.
 * 
 * The String is purged of any non-Integer values and if no integers are entered
 * then the user is asked for input again.
 * 
 * The String is then processed into an Array of the substrings derived
 * when the String is split when a " " is encountered.
 *  
 * The Array of substrings is converted to an ArrayList for ease of processing and an
 * ArrayList of Integers is formed.
 * 
 * The input is then passed to the constructor and a A4c object is instantiated which in turn
 * builds the BST.
 * 
 * The input list is then purged of duplicates and displayed for the user to see, followed by
 * the size of the BST.
 *
 * @param args the arguments
 */
public class A4c extends A4b
{
	A4c(ArrayList<Integer> input)
	{
		super(input);
	}
	
	public static void main(String[] args) 
	{
		Scanner in = new Scanner(System.in);
	
		ArrayList<Integer> input = new ArrayList<>();
		String inputAsString = "";
		
		do 
		{	
			System.out.println(INPUT_PROMPT);
			inputAsString = in.nextLine();
			inputAsString.replaceAll(" ^[0-9]", ""); //removes all non Integer characters
		}
		while(inputAsString.isEmpty());
		
		ArrayList<String> inputAsStringList = new ArrayList<String>(Arrays.asList(inputAsString.split(" ")));
		
			for(String s: inputAsStringList)
			{
				try 
				{
					input.add(Integer.parseInt(s.trim()));
				}
				catch(NumberFormatException e) 
				{
					
				}
			}
		
		in.close();
		
		A4c c = new A4c(input);
		
		try 
		{
			TimeUnit.MILLISECONDS.sleep(50);
		} 
		catch (InterruptedException e) 
		{
			
		}

		input = c.clearDupes(input);
		
		System.out.println("Numbers inputted (excluding duplicates): " + input.toString() + System.lineSeparator());
		System.out.println("Number of nodes in BST: " + c.getSize());
	}
}
