/**
 * Name:		Irfan Virani	
 * Email:		ivira979@mtroyal.ca
 * Course:		COMP 2503
 * Instructor:	Sajida Faiyaz
 * Assignment:	3
 * Part:		B
 * Due Date:	February 18, 2019
 */

import java.util.*;

/**
 * The Class A3b.
 */
public class A3b 
{
	/** The first addend as a String. */
	private String addend1 = "";
	
	/** The first addend as a stack of integers. */
	private Stack<Integer> addend1AsStack = new Stack<Integer> ();
	
	/** The second addend as a String. */
	private String addend2 = "";
	
	/** The second addend as a stack of integers. */
	private Stack<Integer> addend2AsStack = new Stack<Integer> ();
	
	/** The sum of the two addends as a String. Starts empty */
	private String sum = "";
	
	/** The Constant NO_INPUTS_ERR. */
	private static final String NO_INPUTS_ERR = "No inputs given! Try again!";
	
	/** The Constant LESS_THAN_2_INPUTS_ERR. */
	private static final String LESS_THAN_2_INPUTS_ERR = "Insufficent number of inputs. Adding by 0!";
	
	/** The Constant MORE_THAN_2_INPUTS_ERR. */
	private static final String MORE_THAN_2_INPUTS_ERR = "Too many inputs! Only the first 2 will be counted!";
	
	/**
	 * The main method.
	 * Receives input from the user, calls the constructor with the input and calls the 
	 * calculate sum and toString methods.
	 * 
	 * @param args the arguments
	 */
	public static void main(String[] args) 
	{ 
		printPrompt();
		
		Scanner in = new Scanner(System.in);	
		String inputAsString = in.nextLine();
		
		while(inputAsString.length() == 0 ) // forces the user to provide at least 1 input
		{
			System.err.println(NO_INPUTS_ERR);
			inputAsString = in.nextLine();
		}
			
		in.close();
		
		A3b adder = new A3b(inputAsString);
		adder.calculateSum();
		
		System.out.println(adder.toString());
	} 

	/**
	 * Instantiates a new A3b by splitting the input String into two 
	 * substrings which are placed in an ArrayList for each of operation.
	 * The substrings are then pushed onto the corresponding addend Stack.
	 * 
	 * The constructor also verifies user input to ensure the correct amount
	 * of arguments. 
	 * 
	 * If greater than 2 inputs are provided, it is assumed that the user
	 * wants to use the first 2 inputs and processing continues. The appropriate 
	 * error message is displayed to standard err.
	 * 
	 * If less than 2 inputs are provided, it is assumed that the user wants
	 * to add by 0 and processing continues. The appropriate error message is
	 * displayed to standard err.
	 *
	 * @param inputStr the concatenated String of both inputs to be split.
	 */
	public A3b(String inputStr) 
	{
		ArrayList<String> inputAsStringList = new ArrayList<String>(Arrays.asList(inputStr.split(" ")));
		
		if(inputAsStringList.size() < 2) 
		{
			System.err.println(LESS_THAN_2_INPUTS_ERR + System.lineSeparator());
			inputAsStringList.add("0");
		}
		if(inputAsStringList.size() > 2) 
		{
			System.err.println(MORE_THAN_2_INPUTS_ERR + System.lineSeparator());
		}

		for(int j = 0; j < (inputAsStringList.get(0)).length(); j++) 
		{
			this.addend1AsStack.push(Character.getNumericValue((inputAsStringList.get(0).charAt(j))));
		}
		
		for(int j = 0; j < (inputAsStringList.get(1)).length(); j++) 
		{
			this.addend2AsStack.push(Character.getNumericValue((inputAsStringList.get(1).charAt(j))));
		}
	}

	/**
	 * Converts the Stack representation of the first addend to a String (in reverse order).
	 * 
	 * This is done recursively to preserve the contents of the addend Stack without the need of a
	 * temporary Stack to hold the result.
	 */
	private void addend1ToString() 
	{	
		if(this.addend1AsStack.empty()) 
		{
			return;
		}
		
		int temp = addend1AsStack.pop();
		this.addend1 += temp;
		addend1ToString();
		addend1AsStack.push(temp);
	}
	
	/**
	 * Converts the Stack representation of the second addend to a String (in reverse order).
	 * 
	 * This is done recursively to preserve the contents of the addend Stack without the need of a
	 * temporary Stack to hold the result.
	 * 
	 */
	private void addend2ToString() 
	{
		if(this.addend2AsStack.empty()) 
		{
			return;
		}
		
		int temp = addend2AsStack.pop();
		this.addend2 += temp;
		addend2ToString();
		
		addend2AsStack.push(temp);
	}
	
	/**
	 * Calculates sum of the two addend Stacks and appends the result to the String sum.
	 */
	public void calculateSum() 
	{
		this.addend1ToString(); //called prior to calculation as the addend Stacks are modified below.
		this.addend2ToString(); //
		
		int carry = 0;
		int sum = 0;
		
		while(!addend1AsStack.isEmpty() | !addend2AsStack.isEmpty()) 
		{
			sum = nextStackDigit(this.addend1AsStack) + nextStackDigit(this.addend2AsStack) + carry;
			this.sum += Integer.toString(sum % 10);
			carry = sum / 10;
		}
		if(carry != 0)
		{
			this.sum += Integer.toString(carry);
		}
	}

	/**
	 * Reverses the String representation of the first addend 
	 * so that it is in the correct order for printing.
	 *
	 * @return the String addend1 in reverse order
	 */
	private String getAddend1() 
	{
		StringBuilder str = new StringBuilder();
		str.append(addend1);
		addend1 = str.reverse().toString();
		
		return addend1;
	}
	
	/**
	 * Reverses the String representation of the second addend 
	 * so that it is in the correct order for printing.
	 *
	 * @return the String addend2 in reverse order
	 */
	private String getAddend2() 
	{
		StringBuilder str = new StringBuilder();
		str.append(addend2);
		addend2 = str.reverse().toString();
		
		return addend2;
	}

	/**
	 * Reverses the String representation of the sum 
	 * so that it is in the correct order for printing.
	 *
	 * @return the String sum in reverse order
	 */
	private String getSum() 
	{
		StringBuilder str = new StringBuilder();
		str.append(sum);
		sum = str.reverse().toString();
		
		return sum;
	}
	
	/**
	 * Gets the next digit from the top of Stack if it is not empty.
	 * If it is empty, 0 is returned instead. 
	 *
	 * @param addend the addend
	 * @return the Integer digit from the stack, or 0.
	 */
	public static int nextStackDigit(Stack<Integer> addend)
	{
		if(addend.isEmpty()) 
		{
			return 0;
		}
		else
		{
			return addend.pop();
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 * 
	 * Converts the formatted output comprised of the two addends 
	 * and the sum to a String for easy printing.
	 */
	@Override
	public String toString() 
	{
		StringBuilder str = new StringBuilder();
		final int FIELD_WIDTH = (this.sum.length() + 1);
		
		str.append("  ");
		for(int i = 0; i < FIELD_WIDTH - this.addend1.length() ; i++) 
		{
			str.append(" ");
		}
	
		str.append(this.getAddend1() + System.lineSeparator());
		str.append(" +");
		
		for(int i = 0; i < FIELD_WIDTH - this.addend2.length() ; i++)
		{
			str.append(" ");
		}
		
		str.append(this.getAddend2() + System.lineSeparator());
		str.append(" = ");
		str.append(this.getSum() + System.lineSeparator());
		
		return str.toString();
	}
	
	/**
	 * Prints the prompt welcoming the user and requesting the input.
	 */
	public static void printPrompt() 
	{
		System.out.println("Welcome to Long Addition Calculator!" + System.lineSeparator());
		System.out.print("Please enter two positive integers followed by the ENTER/RETURN key: ");
	}
}
	
	

