import java.util.*;

public class AVLNode 
{
	
	AVLNode left, right;
	int data;
	int height;
	
	AVLNode()
	{
		left = right = null;
		data = height = 0;
	}
	
	AVLNode(int n)
	{
		
		left = right = null;
		data = n;
		height = 0;
	}
	
}
