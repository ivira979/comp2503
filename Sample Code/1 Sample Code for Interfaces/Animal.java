
/**
 * Interface is a collection of abstract methods. A class implements an interface
 * and therefore inherits the abstract methods of the interface.
 * 
 * An interface is not a class. Writing an interface is similar to writing a class, 
 * but they are two different concepts. A class describes the attributes and behaviors 
 * of an object. An interface contains behaviors that a class implements.
 * 
 * However, an interface is different from a class in several ways, including:
 *      You cannot instantiate an interface. 
 *      An interface does not contain any constructors.
 *      All of the methods in an interface are abstract.
 *      An interface is not extended by a class; it is implemented by a class.
 *      An interface can extend multiple interfaces.
 */
public interface Animal
{
    public void eat();
    public void travel();

}
