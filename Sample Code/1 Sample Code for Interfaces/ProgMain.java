
/**
 *
 */
public class ProgMain
{
   public static void main(String args[]) {
      Mammal m = new Mammal();
      NonMammals n = new NonMammals();
     
     
      m.eat();
      n.travel();
      System.out.print(m.noOfLegs());
      
      //below gives a compiler error because we can not instantiate interface
     // Animal a = new Animal();
   }
}
