
/**
 */
public class Mammal implements Animal, Others
{
 public void eat() {
      System.out.println("Mammal eats");
   }

   public void travel() {
      System.out.println("Mammal travels");
   } 

   public int noOfLegs() {
      return 0;
   }
   
   public void eatNothing(){
       System.out.println ("Eat nothing");
    }

}
