/**
 */
public class NonMammals implements Animal
{
 public void eat() {
     
     System.out.println("Non Mammal eats");
   }

   public void travel() {
      System.out.println("Non Mammal travels");
   } 

   public int noOfLegs() {
      return 4;
   }

}