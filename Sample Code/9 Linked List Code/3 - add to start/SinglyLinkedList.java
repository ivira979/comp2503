import java.util.*;

public class SinglyLinkedList<type>
{
    private Node<type> start;

    public SinglyLinkedList()
    {
        start = null;
    }

    public boolean isEmpty(){ return (start == null); }
    
    
    public void addToStart(type data) 
    { 
        Node<type> nodeToAdd = new Node(data); 
        if(isEmpty())
            start = nodeToAdd;
        else
        {
          nodeToAdd.setNext(start);
          start = nodeToAdd;
        }
    }
    
    public void print()
    {
        Node<type> curr = start;

        System.out.print("Start->");

        while(curr != null)
        {
            System.out.print("["+curr.getData()+"]->");
            curr = curr.getNext();
        }

        System.out.println("null");
    }

}
