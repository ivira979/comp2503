import java.util.*;

public class SinglyLinkedList<type>
{
    private Node<type> start;

    public SinglyLinkedList()
    {
        start = null;
    }

    public boolean isEmpty(){ return (start == null); }
    
    
    public int size(){
        
        Node<type> curr = start;
        int length = 0;

        while(curr != null)
        {
            length++;
            curr = curr.getNext();
        }

        return length;
    }
    
    
    
    
    public void addToEnd(type data) {
        //without the if and else you will get a null pointer 
        //expection when adding the first element.
        
        Node<type> nodeToAdd = new Node(data); 
        if(start != null){
            Node<type> curr = start;

            while(curr.getNext() != null){
               curr = curr.getNext();
            }
            curr.setNext(nodeToAdd);
        }
        else
            start = nodeToAdd;
    }
    
    
    public void addToStart(type data) 
    { 
        Node<type> nodeToAdd = new Node(data); 
        if(isEmpty())
            start = nodeToAdd;
        else
        {
          nodeToAdd.setNext(start);
          start = nodeToAdd;
        }
    }
    
    public void print()
    {
        Node<type> curr = start;

        System.out.print("Start->");

        while(curr != null)
        {
            System.out.print("["+curr.getData()+"]->");
            curr = curr.getNext();
        }

        System.out.println("null");
    }

}
