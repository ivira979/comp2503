public class Example
{
    public static void main(String[] args)
    {
        SinglyLinkedList<Integer> list = 
               new SinglyLinkedList<Integer>();
               
        list.addToStart(1);
        list.addToStart(2);
        list.addToStart(3);
        list.addToEnd(4);
        list.addToEnd(5);
        
        list.print();
        System.out.println ("The size of the list is " + list.size());
    }
}
