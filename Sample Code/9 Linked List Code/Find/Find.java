public class Find
{
    Node<String> head; 

    public Node<String> find( String f) 
    {
        Node<String> curr = head; 
        boolean flag = false;
        while (curr.getNext() != null){
            curr = curr.getNext();
            if (curr.getData().equals(f)){
                flag = true;
                break;
            }
        }

        if (flag == false) {
            curr.setData("Not found");
        }
        return curr;
    }

    public Node<String> delete (String f) 
    {   
        return null;
    }

    public void addInOrder( Node<String> n) 
    {
        if (head == null){
            head = n;
        }
        else {
            if (n.getData().compareTo(head.getData()) <= 0){
                addHead(n);
            }
            else{
                Node<String> curr = head;
                while(curr.getNext() != null && n.getData().compareTo(curr.getNext().getData()) > 0){
                    curr = curr.getNext();
                }
                if (curr.getNext() == null) addTail(n);
                else{
                    n.setNext(curr.getNext());
                    curr.setNext(n);
                }
            }
        }
    }

    public void addHead( Node<String> n) 
    {
        if (isEmpty()){
            head = n;
        }

        else{
            n.setNext(head);
            head = n;
        }
    }

    public void addTail( Node<String> n) 
    {
        if (isEmpty()){
            head = n;
        }

        else{
            Node<String> curr = head;
            while (curr.getNext()!=null){
                curr = curr.getNext();
            }
            curr.setNext(n);
            tail = n;
        }        
    }

    public boolean isEmpty(){
        if (head == null){
            return true;
        }
        else return false;
    }

    public void printList()
    {
        System.out.println();
        Node<String> curr = head;
        while (curr != null) 
        {
            // Visit the node. In this case, print it out. 
            System.out.println(curr.toString());
            curr = curr.getNext();
        }
    }

    public void emptyList()
    {
        
    }

    public void run() 
    {
        head = null;
        tail = null;

        Node<String> a = new Node<String>("Athabasca");
        Node<String> b = new Node<String>("Andromeda");
        Node<String> c = new Node<String>("Hector");
        Node<String> d = new Node<String>("Victoria");

        /**
        addHead(a);
        addHead(b);
        addHead(c);
        addHead(d);
        
        addTail(a);
        addTail(b);
        addTail(c);
        addTail(d);
        **/
        
        
        addInOrder(b);
        addInOrder(c);
        addInOrder(d);
        addInOrder(a);
        
        Node<String> curr = head;
        System.out.print("Head --> ");
        while (curr != null){
            System.out.print("["+curr.toString() + "] --> ");
            curr = curr.getNext();
        }
        System.out.println(" <-- Tail");

        System.out.println(find("Test"));
        System.out.println(find("Hector"));
        System.out.println(find("Athabasca"));
        
    }

    public static void  main(String[] args) 
    {
        Lab4 l = new Lab4();
        l.run(); 
    }
}

