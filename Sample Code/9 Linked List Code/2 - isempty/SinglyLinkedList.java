import java.util.*;

public class SinglyLinkedList<type>
{
    private Node<type> start;

    public SinglyLinkedList()
    {
        start = null;
    }

    public boolean isEmpty(){ return (start == null); }
    
}
