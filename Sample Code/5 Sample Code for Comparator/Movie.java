

public class Movie implements Comparable<Movie> {
    private double rating;
    private String name;
    private int year;

    public int compareTo(Movie m){
        return this.year - m.year;
    }
    
    public Movie(String n, double r, int y) {
        this.name = n;
        this.rating = r;
        this.year = y;
    }
    
    public double getRating() { return rating; }
    
    public String getName()  {  return name; }

    public int getYear()  {  return year;  }

}