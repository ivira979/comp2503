public class MultipleLinkedObjects
{
    public static void main(String args[])
    {
        NodeTwo reference = new NodeTwo();
        NodeTwo tempRef1;
        NodeTwo tempRef2;
        
        tempRef1 = new NodeTwo();
        reference.setNext(tempRef1);
        
        tempRef2 = new NodeTwo();
        tempRef1.setNext(tempRef2);
        
        tempRef1=tempRef2=null;
    }
}
