public class ClassActivity2
{
    public static void main(String args[])
    {
        NodeTwo reference = new NodeTwo();      
        NodeTwo tempRef1 = null;
        NodeTwo tempRef2 = null;
       
        tempRef1 = new NodeTwo();
        reference.setNext(tempRef1);
        
        tempRef1 = new NodeTwo();
        tempRef2 = reference.getNext();
        tempRef2.setNext(tempRef1);
        
        tempRef1.setNext(reference);
        tempRef1=null;
    }
}

