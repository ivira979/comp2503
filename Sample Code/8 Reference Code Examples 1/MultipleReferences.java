public class MultipleReferences
{
    public static void main(String args[])
    {
        NodeOne reference = new NodeOne();
        NodeOne reference2 = reference;
        NodeOne reference3 = reference;
    }
}

