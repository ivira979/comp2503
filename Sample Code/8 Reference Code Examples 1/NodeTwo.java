public class NodeTwo
{
    private int data;
    private NodeTwo next = null;
    
    public int getData() 
           { return data; }
    public void setData(int value) 
           { data = value; }
           
    public void setNext(NodeTwo other)
           { next = other; }
           
    public NodeTwo getNext() 
           { return next; }
}

