
public class Example1{
    public static void main(String[] args){
        
        //== operator
        //equals()
        
        String s1 = new String ("abc");
        String s2 = new String ("abc");
        
        // == does not compare the content stored inside. It compares the reference (where in memory
        //it is. s1 and s2 are at different references.
        if (s1 == s2)
          System.out.println ("equal");
        else
           System.out.println ("not equal");
       
        //equals gives the exact expected result   
        // equals checks if the objects are the same
       
        if (s1.equals(s2))
          System.out.println ("equal");
        else
           System.out.println ("not equal");   
           
        String s3 = s1;
        if (s1.equals(s3))
          System.out.println ("equal");
        else
           System.out.println ("not equal");   
        
    }
    
}