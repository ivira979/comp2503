
public class Trace5
{
    public void func(int num, int[] list)
    {
        if(num < list.length)
          {
              list[num] = list[num-1] + num;
              func(num+1,list);
          }
    }
    public static void main(String[] args)
    {
        int[] nums = new int[4];
       
        nums[0]=0;
        
        Trace5 trace = new Trace5();
        
        trace.func(1,nums);
        for(int num : nums)
          System.out.println(num);
    
    }
}
