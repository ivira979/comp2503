
public class Trace3
{
    public void func(int num)
    {
        if (num > 1)
            func(num-1); 

        for (int count = 0; count < num; count++)   
            System.out.print("*");
        System.out.println();
    }

    public static void main(String[] args)
    {
        Trace3 trace = new Trace3();
        trace.func(4);
    }
}
