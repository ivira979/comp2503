public class LinkListRun
{
    public static void main(String[] args)
    {
      SinglyLinkedList<Integer> list = new SinglyLinkedList<Integer>();
      
      for(int count=10; count > 0; count--)
         list.addToStart(count);
        
      System.out.println("Print forwards non rec");
      list.printIterative();
      System.out.println("Print forwards rec");
      list.printRecursive();
    }
}
