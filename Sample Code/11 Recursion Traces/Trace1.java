
public class Trace1
{
    public int func(int num1, int num2)
    {
        int result=0;
        if(num2 < 1) result = 1;
        else result = func(num1, num2-1) * num1;
        return result;
    }
    
    public static void main(String[] args)
    {
        Trace1 trace = new Trace1();
        System.out.println("(1) "+trace.func(2,3));
        System.out.println("(2) "+trace.func(2,4));
    }
}
