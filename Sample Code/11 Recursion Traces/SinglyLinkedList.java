
import java.util.*;

public class SinglyLinkedList<type>
{
    private Node<type> start;

    public SinglyLinkedList()
    {
        start = null;
    }

    public boolean isEmpty(){ return (start == null); }

    public void addToStart(type data) 
    { 
        Node<type> nodeToAdd = new Node<type>(data); 
        if(isEmpty())
            start = nodeToAdd;
        else
        {
            nodeToAdd.setNext(start);
            start = nodeToAdd;
        }
    }

    public void printIterative()
    {
        Node<type> mover = start;
        while(mover != null)
        {
            System.out.print("["+mover.getData()+"]");
            mover = mover.getNext();
        }
        System.out.println();
    }

    
    
    public void printRecursive()
    {
       printRec(start);
    }
    
    private void printRec(Node<type> mover)
    {
        if(mover != null)
        {
            System.out.print("["+mover.getData()+"]");
            printRec( mover.getNext() );
            
                   
        }
    }
    

    

}
