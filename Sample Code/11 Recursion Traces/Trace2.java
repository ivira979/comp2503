


public class Trace2
{
    public void func(int num)
    {
        for (int count = 0; count < num; count++)   
            System.out.print("*");
        System.out.println();

        if (num > 1)
            func(num-1);    
    }
    
    public static void main(String[] args)
    {
        Trace2 trace = new Trace2();
        trace.func(4);
    }
}
