
public class TraceHard
{
    public int inception(int i, int[] list) {
        int result = 0;
        if(i <= 0)
            result = i;
        else{
            result += list[i];
            list[i] = inception( i/2 , list);
            list[i] += result;
            System.out.println(list[i]);
        }
        return result;
    }
    public static void main(String args[])    {
        TraceHard trace = new TraceHard();
        int data[] = { 1,2,3,4,5,6,7,8,9,10,11 };
        trace.inception( data.length - 1 , data );

    }
}
