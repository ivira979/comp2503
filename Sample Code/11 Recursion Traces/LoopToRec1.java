public class LoopToRec1
{
    public int loop(int num)
    {
        int sum=0;
        for(int count=0; count < num; count++)
        {
            sum+= count;
        }
        return sum;
    }

    
    public int rec(int count,int num)
    {
        int sum=0;
        if(count < num)
            sum = count + rec(count+1,num);

        return sum;
    }

    public static void main(String[] args)
    {
        LoopToRec1 trace = new LoopToRec1();

        System.out.println("Loop: " + trace.loop(5));
        System.out.println(" Rec: " + trace.rec(0,5));
    }
}
