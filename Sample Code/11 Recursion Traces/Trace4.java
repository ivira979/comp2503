
public class Trace4
{
    public void func(int num, int[] list)
    {
        if(num < list.length)
          {
              list[num] = num;
              func(num+1,list);
          }
    }

    public static void main(String[] args)
    {
        int[] nums = new int[4];
        Trace4 trace = new Trace4();
        
        trace.func(0,nums);
        for(int num : nums)
          System.out.println(num);
    
    }
}
