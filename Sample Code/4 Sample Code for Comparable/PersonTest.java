import java.util.*;

public class PersonTest
{
    public static void main(String args[])
    {
        Person p1 = new Person("ABC", 400);
        Person p2 = new Person("XYZ", 200);
        Person p3 = new Person("TEST", 100);
        
        ArrayList<Person> p = new ArrayList<Person>();
        p.add(p1);
        p.add(p2);
        p.add(p3);
        Collections.sort (p);
        System.out.println (p);
        
        int result = p1.compareTo(p2);

        switch(result)
        {
            case -1: System.out.println("person1 < person2"); break;
            case  0: System.out.println("person1 = person2"); break;
            case  1: System.out.println("person1 > person2"); break;

        }
    }
}
