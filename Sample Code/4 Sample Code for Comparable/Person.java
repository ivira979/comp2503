//comparable interface contains only one method, called compareTo.
//compareTo compares 2 objects, in order to impose an order between them
//returns -1,0,1
public class Person implements Comparable<Person>
{
    private int age;
    private String name;
    public Person(String name, int age)
    {
        this.age = age;
        this.name = name;
    }
    
    //overrides the compareTo method
    public int compareTo (Person other)
    {
      int result = 0;
      if(age < other.age) result = -1;
      else if(age > other.age) result = 1;
      return result;
    }

    public String toString() { return name + " : " + age; }
}
