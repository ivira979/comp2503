/*
 * generic class looks like non-generic class, except the class name is followed by a type parameter
 * token, you can have one or more type parameters
 */

public class Box<T>{
    private T data;
    
    public Box (T value){
        this.data = value;
        
    }
    
    public T get(){
        return data;
    }
    
    
}