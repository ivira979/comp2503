/*
 * 
 * Generic method is a way in which you can eliminate overloaded method. Overloaded method are used
 * to perform the same thing depending on what type of data it is. So instead of having 10 different
 * overload methods, it would be nice to have one method and no matter what data we pass in it will 
 * be nice to have one method so no matter what data we pass in it will do the same thing.
 * 
 */

public class ArrayListExample2{
    public static void main (String[] args){
        Integer[] i = {1,2,3,4};
        Character[] c = {'a', 'c', 'd','e'};
        
        printArr (i);
        printArr (c);
    }
    
    public static void printArr (Integer[] i){
        for (Integer x: i)
            System.out.printf ("%d ",x);
        System.out.println ();    
    }
    
    public static void printArr (Character[] i){
        for (Character x: i)
            System.out.printf ("%s ",x);
        System.out.println ();    
    }
    /*
     * But the problem is we have 2 types of data. But say we have a method that takes
     * String, Long, Short etc.. and instead of having 800 overloaded methods we want
     * one generic method. And thats where generics come in.
     */
    
}