public class BoxTest{
    public static void main (String[] args){
        Box<Integer> i = new Box<Integer> (10);
        Box<String> s = new Box<String> ("Hello world");
        Box<Double> d = new Box<Double> (10.0);
        
        System.out.println ("Integer value is " + i.get());
        System.out.println ("String value is " + s.get());
        System.out.println ("Souble value is " + d.get());
        
        
    }
    
}