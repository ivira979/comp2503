/* remove all the overloaded methods from before.  make a generic method */

public class GenericExample1
{
    public static void main (String[] args){
        Integer[] i = {1,2,3,4};
        Character[] c = {'a', 'c', 'd','e'};
        
        printArr (i);
        printArr (c);
    }
     
    public static <T>  void printArr (T[] i){
        for (T x: i)
            System.out.printf ("%s ",x);
        System.out.println ();  
    }
    
    /* Instead of having a whole bunch of overloaded methods we can pass in this one
     * generic method and pass any data to it and it will work fine. We passed Integer and
     * then Character. 
     * printArr (i) --> it first attempts to location a method that takes integer array for
     * parameter. and then it will look for generic method, and if i can not find out, you will
     * get an error.
     * 
     * Power of generics - Take any type of data and do the same thing
     * 
     */

}