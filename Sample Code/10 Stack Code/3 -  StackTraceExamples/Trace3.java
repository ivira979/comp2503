import java.util.Stack;


public class Trace3 
{
	public static void main(String[] args)
	{
		int[] list = {2,3,6,7,9,10,5};
	    Stack<Integer> s1 = new Stack<Integer>();	
	    Stack<Integer> s2 = new Stack<Integer>();
	    Stack<Integer> s3 = new Stack<Integer>();
		
	    System.out.print("Start: list [ ");
	    for(int n : list)
	    {
	    		System.out.print(" "+ n);
	    		if((n%2) == 0) s1.push(n);
	    		else s2.push(n);
	    }
	    System.out.println(" ]");
	    
	    System.out.println("   S1:"+s1);
	    System.out.println("   S2:"+s2);
	    
	    
	    while(!s1.isEmpty())
	    {
	    	int num = s1.pop();
	    	int num2 = s2.pop();
	    	for(int i =0; i < num; i++)
	    		     s3.push(num2);
	    }
	   
	    System.out.println("   S3:"+s3);
	}
}

