import java.io.CharConversionException;
import java.util.Stack;


public class Trace4 
{
	
	public boolean test(String s)
	{
		boolean result = true;
		char[] characters = s.toCharArray();
		Stack<Character> s1 = new Stack<Character>();
		
		for(char c : characters)
			s1.push(c);
		
		for(char c: characters)
		{
			char c2 = s1.pop();
			if(c != c2)
				result = false;
		}
	
		return result;
	}
	
	public static void main(String[] args)
	{
      String[] testData = {
    		               "aabaa", "aab", "RACECAR",
    		               "CIVIC", "car", "Dog"
    		               };
      
      Trace4 t4 = new Trace4();
      
      for( String s: testData)
      {
    	if(t4.test(s))
    		System.out.print("[PASS]");
    	else
    		System.out.print("[FAIL]");
    		
    	System.out.println(" : " + s);
      }
      
	}
	
	
}
