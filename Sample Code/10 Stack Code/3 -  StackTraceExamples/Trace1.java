import java.util.Stack;


public class Trace1 
{

	public static void main(String[] args)
	{
	     int[] list = {2,3,6,7,9,10,5};
	    Stack<Integer> s = new Stack<Integer>();	
		
	    System.out.print("Before: list [ ");
	    for(int n : list)
	    {
	    		System.out.print(" "+ n);
	        	s.push(n);
	    }
	    System.out.println(" ]");
	    
	    System.out.print(" After: list [ ");
	    while(!s.isEmpty())
	    {
	    	int value = s.pop();
	    	System.out.print(" "+ value);
	    }
	    System.out.println(" ]");
	    
	   
	    
	    
	}
	
}
